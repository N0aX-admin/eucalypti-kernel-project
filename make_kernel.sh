echo BuildKernelv1 
echo Building Multiboot Stuff
nasm -felf32 kernel/boot/multiboot_entry.s -o build/multiboot_entry.o
echo Building Kernel Core 
i686-pc-elf-gcc -ffreestanding -I include -c kernel/init/kinit.c -o build/kinit.o
i686-pc-elf-gcc -ffreestanding -I include -c kernel/video/cga_textmode.c -o build/cga_textmode.o
i686-pc-elf-gcc -ffreestanding -I include -c kernel/io/printk.c -o build/io-combined.o
i686-pc-elf-gcc -ffreestanding -I include -c kernel/io/pic.c -o build/pic.o
i686-pc-elf-gcc -ffreestanding -I include -c kernel/io/io.c -c -o build/io.o
i686-pc-elf-gcc -ffreestanding -I include -c kernel/klibc/string.c -o build/string.o
echo Linking.....
i686-pc-elf-ld -T kernel/link.ld -o bin/kernel.bin -nostdlib build/multiboot_entry.o build/kinit.o build/cga_textmode.o build/io-combined.o build/io.o build/pic.o build/string.o
