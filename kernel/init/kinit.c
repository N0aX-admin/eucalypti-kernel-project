//K_init, entry point for the kernel from what ever (multiboot, ex)
#include<stdint.h>
#include<io/printk.h>
#include<configuration.h>
#include<io/pic.h>
#include<init/multiboot_defines.h>
#include<klibc/string.h>
#include<vm/memory_descriptor.h>
uint8_t str_buffer[10];
struct global_memory glbl_mem;
void dostuff(uint32_t a){
}
void kernel_entry(){
  init_printk();
  printk(LOG_NOTHING, BOOT_HEADER);
  #ifdef MULTIBOOT_LOADED
  extern uint32_t multiboot_data; 
  struct multiboot_info *multiboot_info_struct = (struct multiboot_info*)(multiboot_data);
  u_itoa(multiboot_info_struct->mem_high, &str_buffer[0]);
  /*(Technically It Is The *Address* Space Size Above Traditional Real Mode Boundaries)*/
  uint8_t physical_mem_message[60] = "[Physical Memory Available(KB)]:"; 
  strcat(&physical_mem_message, &str_buffer);
  printk(LOG_WARNING, &physical_mem_message[0]);
  uint32_t mem_entry_ctr, loop_run_flag;
  struct memory_map *mem_map_entry = (struct memory_map*)multiboot_info_struct->memory_entry_addr;
  uint32_t entry_ctr=1;
  while(entry_ctr>5){
    memset(&str_buffer, 0, 10);
    printk(LOG_GOOD,"[Physical Address Region Entry]");
    if(mem_map_entry[entry_ctr].size==24){
      printk(LOG_GOOD, "BAD");
    }
    printk_uint32("[Memory Region Size]: 0x", mem_map_entry[entry_ctr].length);
    printk_uint32("[Memory Region Base]: 0x", mem_map_entry[entry_ctr].base_addr);
    printk_uint32("[Memory Region Type]: 0x", mem_map_entry[entry_ctr].memory_type);
  }
  printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR1");
 printk(LOG_GOOD, "DLTR2");
 printk(LOG_GOOD, "DLTR3");
 printk(LOG_GOOD, "DLTR4");
printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR1");
 printk(LOG_GOOD, "DLTR2");
 printk(LOG_GOOD, "DLTR3");
 printk(LOG_GOOD, "DLTR4");
printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR");
 printk(LOG_GOOD, "DLTR1");
 printk(LOG_GOOD, "DLTR2");
 printk(LOG_GOOD, "DLTR3");
 printk(LOG_GOOD, "DLTR64");
  printk(LOG_GOOD, "Virtual Memmory Mapping Subsystem Loaded [OK]");
  #endif
  asm("cli");
  asm("hlt");
}


