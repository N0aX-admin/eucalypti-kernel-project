#include <stdint.h>
#include <video/video_attr.h>
#include <video/textmode_video.h>
#include <klibc/string.h>
uint32_t magic_ceil(uint32_t numer, uint32_t denom);
void line_copy(uint32_t target_line, uint32_t input_line);
union text_block{
  uint16_t block_full;
  struct{
    uint16_t character : 8;
    uint16_t forecolor : 4;
    uint16_t backcolor : 4;
  };
};
uint32_t cur_position_linear, line_counter, first_jm;
uint32_t cur_x, cur_y;
uint32_t base_line = VID_TXT_HEIGHT;
uint32_t cur_line = 0;//=VID_BOOT_BEGIN_LINE;
void VGATXT_print_character(enum VGATXT_OUT_COLOR text_color, enum VGATXT_OUT_COLOR back_color,
		     uint8_t *character, uint32_t x_target, uint32_t y_target){
  cur_x = x_target;
  cur_y = y_target;
  union text_block block_descriptor;
  block_descriptor.character = (uint8_t)*character;
  block_descriptor.forecolor = (uint8_t)text_color;
  block_descriptor.backcolor = (uint8_t)back_color;
  uint16_t block_offset = (x_target+(VID_TXT_WIDTH*(y_target)));
 
  *((uint16_t*)VID_BASE_TXT+(block_offset)) = (uint16_t)block_descriptor.block_full;
}
void VGATXT_modify_line(enum VGATXT_OUT_COLOR text_color, enum VGATXT_OUT_COLOR back_color, uint8_t *string_ptr, uint32_t string_size, uint32_t line_id){
  uint32_t *line_base_pointer = ((uint32_t*)((line_id*VID_TXT_WIDTH)));
  uint32_t char_ctr;
  for(char_ctr = 0; char_ctr<string_size;char_ctr++){
    VGATXT_print_character(text_color, back_color, (string_ptr+char_ctr), char_ctr, line_id);
  }
}
//uint16_t* VGATXT_retrieve_line(uint32_t line_id){
//return *((uint16_t*)0xb8000+((uint16_t)VID_TXT_WIDTH*line_id));
//}
void VGATXT_print_line(enum VGATXT_OUT_COLOR text_color, enum VGATXT_OUT_COLOR back_color, uint8_t *string_ptr){ //Strings must be eighty characters
  if(cur_line!=VID_TXT_HEIGHT) VGATXT_modify_line(text_color, back_color, string_ptr, strlen(string_ptr), cur_line++); 
  else{
    uint32_t shift_ctr;
    for(shift_ctr=0;shift_ctr<VID_TXT_HEIGHT;shift_ctr++){
      line_copy(shift_ctr-1, shift_ctr);
    }
    VGATXT_modify_line(text_color, back_color, string_ptr, strlen(string_ptr), 24); 
 
  }
}
void VGATXT_clear_scr(){
  uint32_t *video_pointer = (uint32_t*)0xb8000;
  uint32_t i;
  for(i=0; i<2000;i++){    *(video_pointer+i)=0xf;
  }
}
void VGATXT_init_video(){
  cur_position_linear = VID_BASE_TXT;
}
void VGATXT_clear_line(uint8_t line_id){
  uint32_t i;
  for(i=0; i<VID_TXT_WIDTH;i++){
    *((uint32_t*)VID_BASE_TXT+(VID_TXT_WIDTH*line_id)+i) = 0xf;
  }
}
uint32_t REMOVE_shift_down(uint32_t start_ceil){
  uint32_t shift_ctr, shift_selector;
}
void line_copy(uint32_t target_line, uint32_t input_line){
   uint32_t copy_ctr;
     for(copy_ctr=0;copy_ctr<80;copy_ctr++){
       *((uint16_t*)0xb8000+(target_line*80)+copy_ctr) = *((uint16_t*)0xb8000+(input_line*80)+copy_ctr);
     }
}
uint32_t magic_ceil(uint32_t numer, uint32_t denom){
  return ((numer+denom)-1)/denom;
}
