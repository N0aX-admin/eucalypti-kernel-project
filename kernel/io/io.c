#include<io/io.h>
#include<stdint.h>
void outb(uint8_t out_val, uint16_t out_port){
  asm __volatile__("outb %0, %1" : :  "r" (out_val), "r" (out_port));
}
uint8_t inb(uint16_t in_port){
  uint8_t ret_val;
  asm __volatile__("inb  %1, %0" :"=a" (ret_val) : "Nd" (in_port));
  return ret_val;
}
