#include<io/io.h>
#include<stdint.h>
#include<io/pic.h>

void init_8259(){
  //send initalisation control word 0
  union I8259_icw_0 icw_0;
  icw_0.icw4_recv = 1;
  icw_0.pic_cascade = 0;
  icw_0.call_interval_ignore = 0;
  icw_0.edge_triggered = 0;
  icw_0.init_bit = 1;
  icw_0.mcs80_bits = 0; 
  outb(I8259_MASTER_COMMAND_PORT, icw_0.call_point);
  outb(I8259_SLAVE_COMMAND_PORT, icw_0.call_point);
  union I8259_icw_1 icw_1;
  //remap the interrupts from zero to 32 [icw=2]
  icw_1.high_ivt = 32; //master PIC = 32-40 
  outb(I8259_MASTER_DATA_PORT, icw_1.call_point);
  icw_1.high_ivt = 40;//slave PIC = 40-48
  outb(I8259_SLAVE_DATA_PORT, icw_1.call_point);   
  //setup up daisy chaining remapping
  union I8259_icw_2 icw_2;
  icw_2.irq_2_slave = 1;
  outb(I8259_MASTER_DATA_PORT, icw_2.call_point);
  union I8259_icw_2b icw_2b;
  icw_2b.master_irq = 2;
  outb(I8259_SLAVE_DATA_PORT, icw_2b.call_point);
  //setup icw4 == x86 mode
  union I8259_icw_4 icw_4;
  icw_4.x86_mode = 1;
  outb(I8259_MASTER_DATA_PORT, icw_4.call_point);
  outb(I8259_SLAVE_DATA_PORT, icw_4.call_point);
  //Finished 
  outb(I8259_MASTER_DATA_PORT, (uint8_t)0);
  outb(I8259_SLAVE_DATA_PORT, (uint8_t)0);
}
