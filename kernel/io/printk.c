#include <video/textmode_video.h>
#include <configuration.h>
#include <io/printk.h>
#include <klibc/string.h>
#include <io/vsprintk.h>
uint8_t __concat(const uint8_t *input_0, const uint8_t *input_1);
uint8_t vga_buffer[80] = {0}; 
uint8_t use_log_flg = 0;
void printk(enum LOG_LEVEL log_level, const uint8_t *string){
  if(log_level==LOG_GOOD){
    printk_special(LOG_COLOR_WHITE, LOG_COLOR_GREEN, string);
  }
  if(log_level==LOG_WARNING){
    printk_special(LOG_COLOR_WHITE, LOG_COLOR_LIGHT_RED, string);
  }
  if(log_level==LOG_ERROR){
    printk_special(LOG_COLOR_WHITE, LOG_COLOR_RED, string);
  }
  if(log_level==LOG_NOTHING){
    printk_special(LOG_COLOR_WHITE, LOG_COLOR_BLACK, string);
  }
}
void printk_special(enum LOG_TEXT_COLOR forecolor, enum LOG_TEXT_COLOR back_color, const uint8_t *string){
  #ifdef VGA_TXT_LOGGING
  VGATXT_print_line((enum VGATXT_OUT_COLOR)forecolor, (enum VGATXT_OUT_COLOR)back_color, (uint8_t*) string);
  #endif
}
void vprintk(const char *format, ...){
  
}
void init_printk(){
  #ifdef VGA_TXT_LOGGING
  VGATXT_clear_scr();
  #endif
}
void printk_use_log(uint8_t flag_state){
  use_log_flg = flag_state;
}
void printk_uint32(const uint8_t *str, uint32_t a){
#ifdef VGA_TXT_LOGGING
  uint8_t line_buffer[60] = "";
  uint8_t uint_buffer[10] = "";
  memcpy((uint8_t*)line_buffer, (uint8_t*)str, strlen(str));
  u_itoa(a, uint_buffer);
  strcat(line_buffer, uint_buffer);
  printk(LOG_NOTHING, &line_buffer[0]);
#endif
}
void printk_hex32(){

}
uint8_t __concat(const uint8_t *input_0, const uint8_t *input_1){
  
}//writes to longstore
 
