	;; Multiboot_entry.s, multiboot stack + header for linking
	;; Copyright Noah Whiteis 2014
MULTIBOOT_MAGICAL 	equ 	0x1BADB002
MULTIBOOT_4k_GRANULARITY	equ 	1<<0
MULTIBOOT_MEM_DISCOVER 		equ 	1<<1
MULTIBOOT_VIDEO_INFO		equ 	0<<2 ;Disabled for now
MULTIBOOT_FLAGS			equ MULTIBOOT_4k_GRANULARITY | MULTIBOOT_MEM_DISCOVER | MULTIBOOT_VIDEO_INFO
MULTIBOOT_CHECKSUM		equ -(MULTIBOOT_MAGICAL+MULTIBOOT_FLAGS)

section .multiboot_header
multiboot_header:
.magic_id: dd	MULTIBOOT_MAGICAL
.flags:	dd	MULTIBOOT_FLAGS
.checksum: dd	MULTIBOOT_CHECKSUM

section .text
global multiboot_init
multiboot_init:
	mov [multiboot_data], ebx
	mov esp, stack_bottom
	extern kernel_entry
	jmp kernel_entry
return_trampoline:
	hlt
section .data
global multiboot_data
multiboot_data:	dd 0
section .stack
stack_top:
	times 8096 db 0
stack_bottom:
	
