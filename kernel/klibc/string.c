/*
  Not a full stdlib implementation...
 */
#include<stdint.h>
#include<klibc/string.h>
void mod_reverse(uint8_t *s);
int32_t memcmp(const void *ptr1, const void *ptr2, uint32_t size){
  uint32_t compare_ctr;
  for(compare_ctr=0;compare_ctr<size;compare_ctr++){
    if(*((uint8_t*)ptr1+compare_ctr) > *((uint8_t*)ptr2+compare_ctr)){
      return 1;
    }
    if(*((uint8_t*)ptr2+compare_ctr) > *((uint8_t*)ptr1+compare_ctr)){
      return -1;
    }
  }
  return 0;
}
void memcpy(void *dest, const void *src, uint32_t size){
  uint32_t cpy_ctr = 0;
  for(cpy_ctr=0; cpy_ctr<size; cpy_ctr++){
    *((uint8_t*)dest+cpy_ctr) = *((uint8_t*)src+cpy_ctr);
  }
}
void memmove(void *dest, const void *src, uint32_t size){
  uint32_t move_counter;
  for(move_counter=0;move_counter<size;move_counter++){
    *((uint8_t*)dest+move_counter) = *((uint8_t*)src+move_counter);
  }
  for(move_counter=0;move_counter<size;move_counter++){
    *((uint8_t*)src+move_counter) = 0;
  }
  }
void strcpy(uint8_t *dest, const uint8_t *src){
  uint32_t str_ctr;
  for(str_ctr=0;str_ctr<strlen(src);str_ctr++){
    dest[str_ctr] = src[str_ctr];
  }
}
void strcat(uint8_t *dest, const uint8_t *src){
  uint32_t cat_ctr;
  uint32_t dest_size = strlen(dest);
  for(cat_ctr=0;cat_ctr<strlen(src);cat_ctr++){
    dest[dest_size+cat_ctr] = src[cat_ctr];
  }
}
uint32_t strlen(const uint8_t *string){
  uint32_t ictr = 0;
  while(string[ictr]!='\0'){
      ictr++;
  }
  return ictr;
} 
void u_itoa(uint32_t in, uint8_t *out_buffer){
  uint32_t char_ctr=0;
  uint32_t in_temp = in;
  while(in > 0){
    out_buffer[char_ctr++] = ('0'+(in%10));
    in = in / 10;
  }
  if(in_temp!=0) mod_reverse(out_buffer);
}
void uh_itoa(uint32_t in, uint8_t *out_buffer){ 
	//unsigned hex itoa
	uint32_t char_ctr;
	uint32_t in_temp = in;
	while(in > 0){
		if(in%16>9){
			out_buffer[char_ctr++] = ('A'+(in%16));
		}
		else{
			out_buffer[char_ctr++] = ('0'+(in%16));
		}
	}
}
void mod_reverse(uint8_t *in_buffer){
   uint32_t i, j;
   uint8_t temp;
   i=j=temp=0;
   j=strlen(in_buffer)-1;
   for (i=0; i<j; i++, j--){
     temp=in_buffer[i];
     in_buffer[i]=in_buffer[j];
     in_buffer[j]=temp;
   }
   
}
void memset(void *dest, uint8_t val, uint32_t size){
  uint32_t mem_ctr;
  for(mem_ctr=0;mem_ctr<size;mem_ctr++){
    *((uint8_t*)dest+mem_ctr) = val;
  }
}
