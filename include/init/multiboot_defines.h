#ifndef MULTIBOOT_DEFINES_H
#define MULTIBOOT_DEFINES_H
#include<stdint.h>
enum MEMORY_TYPE{ 
  MEMORY_RAM=1, RESERVE=2, ACPI_MEM=3, ACPI_NVS=4, BAD_REGION=5
};
struct multiboot_flags{

};
struct memory_map{
  uint32_t size;
  uint64_t base_addr;
  uint64_t length;
  uint32_t memory_type;
};
struct multiboot_drives{
  uint32_t structure_size;
  uint32_t drive_id;    //BIOS id
  uint8_t drive_mode;   //CHS=0 LBA=1
  uint8_t drive_heads;
  uint8_t drive_sectors;
};
struct multiboot_info{
  uint32_t flags; 
  uint32_t mem_low;
  uint32_t mem_high;
  uint32_t boot_device; //taken from bios 0x80=ATA/HDD 0x0=FDD ex..
  uint32_t cmdline; 
  uint32_t mods_count; //module_count
  uint32_t mods_addr;
  uint32_t multiboot_sym_tbl[4];
  uint32_t memory_map_size;
  uint32_t memory_entry_addr;
  uint32_t drive_tbl_length;;
  struct multiboot_drives *mboot_device;
};
#endif
