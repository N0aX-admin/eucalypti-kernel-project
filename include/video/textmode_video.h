#ifndef _TEXTMODE_VIDEO_H
#define _TEXTMODE_VIDEO_H
#include<stdint.h>
enum VGATXT_OUT_COLOR{
  COLOR_BLACK, COLOR_BLUE, COLOR_GREEN, COLOR_CYAN, COLOR_RED,
  COLOR_MAGENTA, COLOR_BROWN, COLOR_GREY, COLOR_DARK_GREY, COLOR_LIGHT_BLUE       , COLOR_LIGHT_GREEN, LIGHT_LIGHT_CYAN, COLOR_LIGHT_RED, 
  COLOR_LIGHT_MAGENTA, COLOR_LIGHT_BROWN, COLOR_WHITE
};
void VGATXT_clear_scr();
void VGATXT_print_character(enum VGATXT_OUT_COLOR text_color, enum VGATXT_OUT_COLOR back_color, uint8_t *character, uint32_t x_target, uint32_t y_target);
void VGATXT_clear_line(uint8_t line_id);
void VGATXT_modify_line(enum VGATXT_OUT_COLOR text_color, enum VGATXT_OUT_COLOR back_color, uint8_t *string_ptr, uint32_t string_size, uint32_t line_id);
void VGATXT_print_line(enum VGATXT_OUT_COLOR text_color, enum VGATXT_OUT_COLOR back_color, uint8_t *string_ptr);
#endif
