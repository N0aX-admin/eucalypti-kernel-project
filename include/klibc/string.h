#ifndef STRING_H
#define STRING_H
#include<stdint.h>
int32_t memcmp(const void *ptr1, const void *ptr2, uint32_t size);
void memcpy(void *dest, const void *src, uint32_t size);
void memmove(void *dest, const void *src, uint32_t size);
void strcpy(uint8_t *dest, const uint8_t *src);
void strcat(uint8_t *dest, const uint8_t *src);
uint32_t strlen(const uint8_t *string);
void strcat(uint8_t *dest, const uint8_t *src);
void u_itoa(uint32_t in, uint8_t *out_buffer);
void memset(void *dest, uint8_t val, uint32_t size);
#endif
