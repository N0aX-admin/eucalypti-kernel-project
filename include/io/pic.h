//http://pdos.csail.mit.edu/6.828/2012/readings/hardware/8259A.pdf
#define I8259_MASTER_COMMAND_PORT 0x0020
#define I8259_MASTER_DATA_PORT 0x0021
#define I8259_SLAVE_COMMAND_PORT 0x00A0
#define I8259_SLAVE_DATA_PORT 0x00A1

union I8259_icw_0{
  uint8_t call_point;
  struct{
    uint8_t icw4_recv : 1; //use initalisation control word 4
    uint8_t pic_cascade : 1; //master=0 slave=set
    uint8_t call_interval_ignore :1; //ignore
    uint8_t edge_triggered : 1; //set edge
    uint8_t init_bit : 1; //set 1 for init=0c
    uint8_t mcs80_bits : 3; //used in the old mcs-80 shit/ ignore
  };
};
union I8259_icw_1{
  uint8_t call_point;
  struct{
    uint8_t high_ivt; //High interrupt vector id
      };
};
union I8259_icw_2{ //primary 8259 pic
  uint8_t call_point;
  struct{
    uint8_t irq_0_slave : 1;
    uint8_t irq_1_slave : 1;
    uint8_t irq_2_slave : 1;
  };
};
union I8259_icw_2b{
  uint8_t call_point;
  struct{
    uint8_t master_irq : 3;
    uint8_t reserved_zero : 5;
  };
};
union I8259_icw_4{
  uint8_t call_point;
  struct{
    uint8_t x86_mode : 1;
  };
};
void init_8259();
