#ifndef IO_H
#define IO_H
#include<stdint.h>
void outb(uint8_t out_val, uint16_t out_port);
uint8_t inb(uint16_t in_port);

#endif
