#ifndef _PRINTK_H
#define _PRINTK_H 
enum LOG_TEXT_COLOR{
  LOG_COLOR_BLACK, LOG_COLOR_BLUE, LOG_COLOR_GREEN, LOG_COLOR_CYAN, LOG_COLOR_RED, LOG_COLOR_MAGENTA, LOG_COLOR_BROWN, LOG_COLOR_GREY, LOG_COLOR_DARK_GREY, LOG_COLOR_LIGHT_BLUE , LOG_COLOR_LIGHT_GREEN, LOG_LIGHT_LIGHT_CYAN, LOG_COLOR_LIGHT_RED, LOG_COLOR_LIGHT_MAGENTA, LOG_COLOR_LIGHT_BROWN, LOG_COLOR_WHITE
};
enum LOG_LEVEL{
  LOG_GOOD, LOG_WARNING, LOG_ERROR, LOG_FATAL, LOG_NOTHING
};
void printk(enum LOG_LEVEL log_level, const uint8_t *string);
void printk_special(enum LOG_TEXT_COLOR forecolor, enum LOG_TEXT_COLOR back_color, const uint8_t *string);
void init_printk();
void printk_use_log(uint8_t flag_state);
void printk_uint32(const uint8_t *str, uint32_t a);
#endif
